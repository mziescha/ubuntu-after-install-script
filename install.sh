#!/bin/bash

RELEASE=$(lsb_release -sc)
echo ${RELEASE}
LIBS="vim postgresql gcc g++ perltidy chromium-browser git curl vlc libnss-resolve:i386 xfonts-100dpi xfonts-75dpi"

for LIB in $LIBS
do
    echo ${LIB}
    apt-get install --yes ${LIB}
done 

apt-get update
apt-get -y upgrade
apt-get autoremove

#sublime text
wget c758482.r82.cf2.rackcdn.com/sublime-text_build-3083_amd64.deb
dpkg -i sublime-text_build-3083_amd64.deb 

#virtualbox
add-apt-repository "deb http://download.virtualbox.org/virtualbox/debian vivid contrib"
wget -q https://www.virtualbox.org/download/oracle_vbox.asc -O- | apt-key add -
apt-get update
apt-get install -y virtualbox-5.0
apt-get install -y dkms

#dropbox
add-apt-repository "deb http://linux.dropbox.com/ubuntu ${RELEASE} main"
apt-key adv --keyserver pgp.mit.edu --recv-keys 5044912E
apt-get update
apt-get install -y dropbox

#refresh sources
rm /var/lib/apt/lists/* -vf
apt-get update

#Steam
wget http://media.steampowered.com/client/installer/steam.deb
dpkg -i steam.deb

#skype
add-apt-repository "deb http://archive.canonical.com/ ${RELEASE} partner"
apt-get update
apt-get install -y skype


#Perl only for cpanm al the other packages as non root on local home
cpan -i App::cpanminus

#rust + cargo
curl -sS https://static.rust-lang.org/rustup.sh | sh

sh -c "echo 'deb http://download.opensuse.org/repositories/isv:/ownCloud:/desktop/Ubuntu_$(lsb_release -rs)/ /' > /etc/apt/sources.list.d/owncloud-client.list"
apt-get update
apt-key add - < Release.key
apt-get update
apt-get install -y owncloud-client

add-apt-repository ppa:obsproject/obs-studio
apt-get update
apt-get install -y obs-studio qtbase5-dev libqt5webkit5-dev pulseaudio-module-x11 pulseaudio:i386 build-essential pkg-config cmake checkinstall


exit

#after steam install as non root user
rm -f ~/.local/share/Steam/ubuntu12_32/steam-runtime/i386/usr/lib/i386-linux-gnu/libgcc_s.so.1
rm -f ~/.local/share/Steam/ubuntu12_32/steam-runtime/i386/usr/lib/i386-linux-gnu/libstdc++.so.6
rm -f ~/.local/share/Steam/ubuntu12_32/steam-runtime/i386/usr/lib/i386-linux-gnu/libstdc++.so.6.0.18
