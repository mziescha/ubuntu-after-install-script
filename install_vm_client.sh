#!/bin/bash

USER=$1
shift

RELEASE=$(lsb_release -sc)
echo "${USER} on ${RELEASE}"

apt-get update
apt-get install -y vim gcc g++ perltidy chromium-browser git curl libnss-resolve:i386 xfonts-100dpi xfonts-75dpi apt-transport-https
apt-get -y upgrade
apt-get autoremove

#sublime text
wget -qO - https://download.sublimetext.com/sublimehq-pub.gpg | apt-key add -
echo "deb https://download.sublimetext.com/ apt/stable/" | tee /etc/apt/sources.list.d/sublime-text.list
apt-get update
apt-get install -y sublime-text

#refresh sources
rm /var/lib/apt/lists/* -vf
apt-get update

#Perl only for cpanm al the other packages as non root on local home
cpan -i App::cpanminus

#rust + cargo
curl -sS https://static.rust-lang.org/rustup.sh | sh

#docker
apt-get install -y linux-image-extra-$(uname -r) linux-image-extra-virtual ca-certificates software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add -
apt-key fingerprint 0EBFCD88
add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu  ${RELEASE} stable"
apt-get update
apt-get install -y docker-ce
usermod -aG docker ${USER}

#open-vpn
apt-get install -y openvpn

echo 'alias dockviz="docker run -it --rm -v /var/run/docker.sock:/var/run/docker.sock nate/dockviz"' >> $HOME/.bashrc
echo 'alias dockviz="docker run -it --rm -v /var/run/docker.sock:/var/run/docker.sock nate/dockviz"' >> ${USER}/.bashrc

init 6
